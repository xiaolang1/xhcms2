<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */
 
 
namespace app\admin\service;
use think\facade\Validate;


class FieldService
{

	public static function saveData($type,$data){
		$rule = [
			'name'  => 'require',
			'field' => 'require',
			'type' => 'require'
		];
		
		$msg = [
			'name.require'  => '字段名称必填',
			'field.require'  => '字段必填',
			'type.require'=>'字段类型必填',
		];
		
		$validate = Validate::rule($rule)->message($msg);	
		if (!$validate->check($data)) {
			throw new ValidateException($validate->getError());
		}
		$data['field'] = strtolower(trim($data['field'])); //字段强制小写
		try{
			if($type == 'add'){
				$info = db('field')->where(['extend_id'=>$data['extend_id'],'field'=>$data['field']])->find();
				if($info) throw new ValidateException('字段已经存在');
				$reset = db('field')->insertGetId($data);	//创建操作字段
				if($reset){
					db('field')->update(['id'=>$reset,'sortid'=>$reset]); //更新排序
				}		
			}elseif($type == 'edit'){
				$res = db('field')->update($data);
			}
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
			
		return $reset;
	}
	
	
	/**
     * 移动排序
	 * @param (输入参数：)  {string}        id 当前ID
     * @param (输入参数：)  {string}        type 类型 1上移 2 下移
     * @return (返回参数：) {bool}        
     * @return bool 信息
     */
	public static function arrowsort($id,$type){
		$data = db('field')->where('id',$id)->find();
		if($type == 1){
			$map = 'sortid < '.$data['sortid'].' and extend_id = '.$data['extend_id'];
			$info = db('field')->where($map)->order('sortid desc')->find();
		}else{
			$map = 'sortid > '.$data['sortid'].' and extend_id = '.$data['extend_id'];
			$info = db('field')->where($map)->order('sortid asc')->find();
		}
		try{
			if($info && $data){
				db('field')->update(['id'=>$id,'sortid'=>$info['sortid']]);
				db('field')->update(['id'=>$info['id'],'sortid'=>$data['sortid']]);
			}else{
				throw new \Exception('目标位置没有数据');
			}
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		 return true;
	}
	
	public function getFieldData($fieldInfo){
		
		switch($fieldInfo['type']){
			
			//文本框
			case 1:
				$str .="					<div class=\"form-group\">\n";
				$str .="						<label class=\"col-sm-2 control-label\">".$fieldInfo['name']."：</label>\n";
				$str .="						<div class=\"col-sm-9\">\n";
				if(!isset($fieldInfo['data_id']) && !isset($fieldInfo['content_id']) && !empty($fieldInfo['default_value'])){
					$defaultValue = $fieldInfo['default_value'];
				}else{
					$defaultValue = $fieldInfo['value'];
				}
				$str .="							<input type=\"text\" id=\"".$fieldInfo['field']."\" value=\"".$defaultValue."\" name=\"".$fieldInfo['field']."\" class=\"form-control\" placeholder=\"请输入".$fieldInfo['name']."\">\n";
				if(!empty($fieldInfo['note'])){
					$str .="							<span class=\"help-block m-b-none\">".$fieldInfo['note']."</span>\n";
				}
				
				$str .="						</div>\n";
				$str .="					</div>\n";
			break;
			
			//下拉框
			case 2:
				$str .="					<div class=\"form-group\">\n";
				$str .="						<label class=\"col-sm-2 control-label\">".$fieldInfo['name']."：</label>\n";
				$str .="						<div class=\"col-sm-9\">\n";
				
				if(!isset($fieldInfo['data_id']) && !isset($fieldInfo['content_id']) && !empty($fieldInfo['default_value'])){
					$defaultValue = $fieldInfo['default_value'];
				}else{
					$defaultValue = $fieldInfo['value'];
				}
				
				$str .="							<select lay-ignore name=\"".$fieldInfo['field']."\" class=\"form-control\" id=\"".$fieldInfo['field']."\">\n";
				$str .="								<option value=\"\">请选择</option>\n";
				$searchArr = explode(',',$fieldInfo['config']);
				if($searchArr){
					foreach($searchArr as $k=>$v){
						$varArr = explode('|',$v);
						if($defaultValue == $varArr[1]){
							$str .= "								<option selected value=\"".$varArr[1]."\">".$varArr[0]."</option>\n";
						}else{
							$str .= "								<option value=\"".$varArr[1]."\">".$varArr[0]."</option>\n";
						}
					}
				}
				
				$str .= "							</select>\n";
				if(!empty($fieldInfo['note'])){
					$str .="							<span class=\"help-block m-b-none\">".$fieldInfo['note']."</span>\n";
				}
				
				$str .="						</div>\n";
				$str .="					</div>\n";
			break;
			
			//单选框
			case 3:
				$str .="					<div class=\"form-group\">\n";
				$str .="						<label class=\"col-sm-2 control-label\">".$fieldInfo['name']."：</label>\n";
				$str .="						<div class=\"col-sm-9\">\n";
				
				if(!isset($fieldInfo['data_id']) && !isset($fieldInfo['content_id']) && !empty($fieldInfo['default_value'])){
					$defaultValue = $fieldInfo['default_value'];
				}else{
					$defaultValue = $fieldInfo['value'];
				}

				$valArr = explode(',',$fieldInfo['config']);
				
				if($valArr){
					foreach($valArr as $k=>$v){
						$varArr = explode('|',$v);
						if($defaultValue == $varArr[1]){
							$str .= "							<input name=\"".$fieldInfo['field']."\" value=\"".$varArr[1]."\" type=\"radio\" checked title=\"".$varArr[0]."\">\n";
						}else{
							$str .= "							<input name=\"".$fieldInfo['field']."\" value=\"".$varArr[1]."\" type=\"radio\" title=\"".$varArr[0]."\">\n";
						}
						
					}
				}
				
				
				if(!empty($fieldInfo['note'])){
					$str .="							<span class=\"help-block m-b-none\">".$fieldInfo['note']."</span>\n";
				}
				
				$str .="						</div>\n";
				$str .="					</div>\n";
			break;
			
			//复选框
			case 4:
				$str .="					<div class=\"form-group\">\n";
				$str .="						<label class=\"col-sm-2 control-label\">".$fieldInfo['name']."：</label>\n";
				$str .="						<div class=\"col-sm-9\">\n";
				
				if(!isset($fieldInfo['data_id']) && !isset($fieldInfo['content_id']) && !empty($fieldInfo['default_value'])){
					$defaultValue = $fieldInfo['default_value'];
				}else{
					$defaultValue = $fieldInfo['value'];
				}
						
				$searchArr = explode(',',$fieldInfo['config']);
				
				if($searchArr){
					foreach($searchArr as $k=>$v){
						$varArr = explode('|',$v);
						if(in_array($varArr[1],explode(',',$defaultValue))){
							$str .= "								<input name=\"".$fieldInfo['field']."\" checked value=\"".$varArr[1]."\" type=\"checkbox\" title=\"".$varArr[0]."\">\n";
						}else{
							$str .= "								<input name=\"".$fieldInfo['field']."\" value=\"".$varArr[1]."\" type=\"checkbox\" title=\"".$varArr[0]."\">\n";
						}
					}
				}
				
				if(!empty($fieldInfo['note'])){
					$str .="							<span class=\"help-block m-b-none\">".$fieldInfo['note']."</span>\n";
				}
				
				$str .="						</div>\n";
				$str .="					</div>\n";
			break;
			
			
			//文本域
			case 6:
				$str .="					<div class=\"form-group\">\n";
				$str .="						<label class=\"col-sm-2 control-label\">".$fieldInfo['name']."：</label>\n";
				$str .="						<div class=\"col-sm-9\">\n";
				if(!isset($fieldInfo['data_id']) && !isset($fieldInfo['content_id']) && !empty($fieldInfo['default_value'])){
					$defaultValue = $fieldInfo['default_value'];
				}else{
					$defaultValue = $fieldInfo['value'];
				}
				$str .="							<textarea id=\"".$fieldInfo['field']."\" name=\"".$fieldInfo['field']."\"  class=\"form-control\" placeholder=\"请输入".$fieldInfo['name']."\">".$defaultValue."</textarea>\n";
				if(!empty($fieldInfo['note'])){
					$str .="							<span class=\"help-block m-b-none\">".$fieldInfo['note']."</span>\n";
				}
				
				$str .="						</div>\n";
				$str .="					</div>\n";
			break;
			
			//日期选择框
			case 7:
				
				if(!isset($fieldInfo['data_id']) && !isset($fieldInfo['content_id'])){
					$defaultValue = date('Y-m-d H:i:s');
				}else{
					if(!empty($fieldInfo['value'])){
						$defaultValue = date('Y-m-d H:i:s',$fieldInfo['value']);
					}
				}
				
				$str .="					<div class=\"form-group\">\n";
				$str .="						<label class=\"col-sm-2 control-label\">".$fieldInfo['name']."：</label>\n";
				$str .="						<div class=\"col-sm-9\">\n";

				$str .="							<input type=\"text\" value=\"".$defaultValue."\" name=\"".$fieldInfo['field']."\"  placeholder=\"请输入".$fieldInfo['name']."\" class=\"form-control\" id=\"".$fieldInfo['field']."\">\n";
				
				
				if(!empty($fieldInfo['note'])){
					$str .="							<span class=\"help-block m-b-none\">".$fieldInfo['note']."</span>\n";
				}
				
				$str .="						</div>\n";
				$str .="					</div>\n";
			break;
			
			//单图上传
			case 8:
				$str .="					<div class=\"form-group\">\n";
				$str .="						<label class=\"col-sm-2 control-label\">".$fieldInfo['name']."：</label>\n";
				$str .="						<div class=\"col-sm-6\">\n";
				if(!isset($fieldInfo['data_id']) && !isset($fieldInfo['content_id']) && !empty($fieldInfo['default_value'])){
					$defaultValue = $fieldInfo['default_value'];
				}else{
					$defaultValue = $fieldInfo['value'];
				}
				$str .="							<input type=\"text\" id=\"".$fieldInfo['field']."\" value=\"".$defaultValue."\" name=\"".$fieldInfo['field']."\" onmousemove=\"showBigPic(this.value)\" onmouseout=\"closeimg()\" class=\"form-control\" placeholder=\"请输入".$fieldInfo['name']."\">\n";
				
				if(!empty($fieldInfo['note'])){
					$str .="							<span class=\"help-block m-b-none\">".$fieldInfo['note']."</span>\n";
				}
				
				$str .="						</div>\n";
				$str .="						<div class=\"col-sm-2\" style=\"position:relative; right:30px;\">\n";
				$str .="							<span id=\"".$fieldInfo['field']."_upload\"></span>\n";
				$str .="						</div>\n";
				$str .="					</div>\n";
			break;
			
			
			//文件上传
			case 10:
				$str .="					<div class=\"form-group\">\n";
				$str .="						<label class=\"col-sm-2 control-label\">".$fieldInfo['name']."：</label>\n";
				$str .="						<div class=\"col-sm-6\">\n";
				if(!isset($fieldInfo['data_id']) && !isset($fieldInfo['content_id']) && !empty($fieldInfo['default_value'])){
					$defaultValue = $fieldInfo['default_value'];
				}else{
					$defaultValue = $fieldInfo['value'];
				}
				$str .="							<input type=\"text\" id=\"".$fieldInfo['field']."\" value=\"".$defaultValue."\" name=\"".$fieldInfo['field']."\" class=\"form-control\" placeholder=\"请输入".$fieldInfo['name']."\">\n";
				
				if(!empty($fieldInfo['note'])){
					$str .="							<span class=\"help-block m-b-none\">".$fieldInfo['note']."</span>\n";
				}
				
				$str .="						</div>\n";
				$str .="						<div class=\"col-sm-2\" style=\"position:relative; right:30px;\">\n";
				$str .="							<span id=\"".$fieldInfo['field']."_upload\"></span>\n";
				$str .="						</div>\n";
				$str .="					</div>\n";
			break;
			
			//xheditor编辑器
			case 11:
				$str .="					<div class=\"form-group\">\n";
				$str .="						<label class=\"col-sm-2 control-label\">".$fieldInfo['name']."：</label>\n";
				$str .="						<div class=\"col-sm-9\">\n";
				if(!isset($fieldInfo['data_id']) && !isset($fieldInfo['content_id']) && !empty($fieldInfo['default_value'])){
					$defaultValue = $fieldInfo['default_value'];
				}else{
					$defaultValue = $fieldInfo['value'];
				}
				$str .="								<textarea id=\"".$fieldInfo['field']."\" name=\"".$fieldInfo['field']."\" style=\"width: 100%; height:300px;\">".$defaultValue."</textarea>\n";
				$str .="								<script type=\"text/javascript\">$('#".$fieldInfo['field']."').xheditor({html5Upload:false,upLinkUrl:\"".url('admin/Upload/editorUpload',['immediate'=>1])."\",upLinkExt:\"zip,rar,txt,doc,docx,pdf,xls,xlsx\",tools:'simple',upImgUrl:\"".url('admin/Upload/editorUpload',['immediate'=>1])."\",upImgExt:\"jpg,jpeg,gif,png\"});</script>\n";
				
				if(!empty($fieldInfo['note'])){
					$str .="							<span class=\"help-block m-b-none\">".$fieldInfo['note']."</span>\n";
				}
				
				$str .="						</div>\n";
				$str .="					</div>\n";
			break;
			
			//货币
			case 13:
				$str .="					<div class=\"form-group\">\n";
				$str .="						<label class=\"col-sm-2 control-label\">".$fieldInfo['name']."：</label>\n";
				$str .="						<div class=\"col-sm-9\">\n";
				if(!isset($fieldInfo['data_id']) && !isset($fieldInfo['content_id']) && !empty($fieldInfo['default_value'])){
					$defaultValue = $fieldInfo['default_value'];
				}else{
					$defaultValue = $fieldInfo['value'];
				}
				$str .="							<input type=\"text\" id=\"".$fieldInfo['field']."\" value=\"".$defaultValue."\" name=\"".$fieldInfo['field']."\" class=\"form-control\" placeholder=\"请输入".$fieldInfo['name']."\">\n";
				if(!empty($fieldInfo['note'])){
					$str .="							<span class=\"help-block m-b-none\">".$fieldInfo['note']."</span>\n";
				}
				
				$str .="						</div>\n";
				$str .="					</div>\n";
			break;
			
			
			//百度编辑器
			case 16:
				$str .="					<div class=\"form-group\">\n";
				$str .="						<label class=\"col-sm-2 control-label\">".$fieldInfo['name']."：</label>\n";
				$str .="						<div class=\"col-sm-9\">\n";
				if(!isset($fieldInfo['data_id']) && !isset($fieldInfo['content_id']) && !empty($fieldInfo['default_value'])){
					$defaultValue = $fieldInfo['default_value'];
				}else{
					$defaultValue = $fieldInfo['value'];
				}
				$str .="							<script id=\"".$fieldInfo['field']."\" type=\"text/plain\" name=\"".$fieldInfo['field']."\" style=\"width:100%;height:300px;\">".$defaultValue."</script>\n";
				$str .="							<script type=\"text/javascript\">\n";
				$str .="								var ue = UE.getEditor('".$fieldInfo['field']."');\n";
				$str .="								scaleEnabled:true\n";
				$str .="							</script>\n";
				if(!empty($fieldInfo['note'])){
					$str .="							<span class=\"help-block m-b-none\">".$fieldInfo['note']."</span>\n";
				}
				
				$str .="						</div>\n";
				$str .="					</div>\n";
			break;
			
		}
		
		return $str;
	}

    
}
