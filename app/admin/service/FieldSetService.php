<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */
 
 
namespace app\admin\service;

class FieldSetService
{

	//字段属性
	public static function typeField(){
		
		$list=array(
            1=> array(
                'name'=>'文本框',
                'property'=>1,
                ),
            2=> array(
                'name'=>'下拉框',
                'property'=>3,
                ),
            3=> array(
                'name'=>'单选框',
                'property'=>3,
                ),
            4=> array(
                'name'=>'多选框',
                'property'=>1,
                ),
            6=> array(
                'name'=>'文本域',
                'property'=>4,
                ),
            7=> array(
                'name'=>'日期框',
                'property'=>2,
                ),
            8=> array(
                'name'=>'单图上传',
                'property'=>1,
                ),
			10=> array(
                'name'=>'文件上传',
                'property'=>4,
                ),
            11=> array(
                'name'=>'编辑器(xheditor)',
                'property'=>4,
                ),
			16=> array(
                'name'=>'编辑器(ueditor)',
                'property'=>4,
                ),
			13=> array(
                'name'=>'货币',
                'property'=>5,
                ),
			20=> array(
                'name'=>'IP',
                'property'=>1,
                ),
			12=> array(
                'name'=>'创建时间(后台录入)',
                'property'=>2,
                )
            
        );
        return $list;
	}
	
	
	
	//字段的sql属性
    public static function propertyField()
    {
        $list=array(
            1=> array(
                'name'=>'varchar',
                'maxlen'=>250,
                'decimal'=>0,
                ),
            2=> array(
                'name'=>'int',
                'maxlen'=>11,
                'decimal'=>0,
                ),
			3=> array(
                'name'=>'smallint',
                'maxlen'=>6,
                'decimal'=>0,
                ),
            4=> array(
                'name'=>'text',
                'maxlen'=>0,
                'decimal'=>0,
                ),
            5 => array(
                'name'=>'decimal',
                'maxlen'=>10,
                'decimal'=>2,
                ),
			6=> array(
                'name'=>'tinyint',
                'maxlen'=>4,
                'decimal'=>0,
                ),
        );
        return $list;
    }
	
	//字段验证规则列表
	public static function ruleList(){
		$list = [
			'邮箱'	=> '/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/',
			'网址'	=> '/^((ht|f)tps?):\/\/([\w\-]+(\.[\w\-]+)*\/)*[\w\-]+(\.[\w\-]+)*\/?(\?([\w\-\.,@?^=%&:\/~\+#]*)+)?/',
			'货币'	=> '/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/',
			'数字'	=> '/^[0-9]*$/',
			'手机号'=> '/^1[345678]\d{9}$/',
			'身份证'=> '/^[1-9]\d{5}(18|19|20|(3\d))\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/',
		];
		return $list;
	}
	
	
}
