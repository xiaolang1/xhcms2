<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\admin\controller;
use app\admin\service\FormDataService;


class FormData extends Admin {

	
	/*数据列表*/
	function index(){
		$extend_id = $this->request->param('extend_id', '', 'intval');	
		if (!$this->request->isAjax()){
			$fieldList =  db('field')->where(['extend_id'=>$extend_id,'status'=>1,'list_show'=>1])->order('sortid asc')->select();
			$searchList = db('field')->where(['extend_id'=>$extend_id,'status'=>1,'search_show'=>1])->order('sortid asc')->select();
			$this->view->assign('extendInfo',db('extend')->where('extend_id',$extend_id)->find());
			$this->view->assign('formStr',FormDataService::getTableList($fieldList));
			$this->view->assign('searchGroup',FormDataService::getSearchGroup($searchList));
			$this->view->assign('queryParam',FormDataService::getQueryParam($searchList));
			$this->view->assign('extend_id',$extend_id);
			$this->view->assign('searchList',json_encode($searchList));
			return view('form_data/index');
		}else{
			$limit  = $this->request->post('limit', 20, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$where= [];
			$searchList = db('field')->where(['extend_id'=>$extend_id,'status'=>1,'search_show'=>1])->select();
			if($searchList){
				foreach($searchList as $k=>$v){
					if($v['type'] == 12){
						$start_time = $this->request->post($v['field'].'_start');
						$end_time = $this->request->post($v['field'].'_end');
						$where[$v['field']] = ['between',[strtotime($start_time),strtotime($end_time)]];
					}else{
						$where[$v['field']] = ['like',input('param.'.$v['field'], '', 'serach_in')];
					}	
				}	
			}
			$extendInfo = db('extend')->where('extend_id',$extend_id)->find();
			if($extendInfo['order_by']){
				$orderby = $extendInfo['order_by']; 
			}else{
				$orderby = 'data_id desc';
			}
			$res = FormDataService::loadList(formatWhere($where),$field='*',$limit,$orderby,$page,$extend_id);
			return json($res);
		}
	}
	
	/*删除数据*/
	function delete(){
		$idx = $this->request->param('data_ids', '', 'strval');
		$extend_id = $this->request->param('extend_id', '', 'strval');
		if(empty($idx) || empty($extend_id)) $this->error('参数错误');
		$where['data_id'] =explode(',',$idx);
		$res = FormDataService::delete($where,$extend_id);
		return json(['status'=>'00','msg'=>'操作成功']);
	}
	
	
	/*创建数据*/
	function add(){
		if (!$this->request->isPost()){
			$extend_id = $this->request->param('extend_id','','intval');
			if(empty($extend_id)) $this->error('参数错误');
			$htmlstr = '';
			$fieldList = db('field')->where(['extend_id'=>$extend_id,'status'=>1])->order('sortid asc')->select();
			foreach($fieldList as $key=>$val){
				$htmlstr .= \app\admin\service\FieldService::getFieldData($val);
			}
			$this->view->assign('extend_id',$extend_id);
			$this->view->assign('formStr',$htmlstr);
			return view('form_data/info');
		}else{
			$data = $this->request->post();
			$fieldList = db('field')->where(['extend_id'=>$data['extend_id'],'status'=>1])->select()->toArray();
			$res = FormDataService::saveData('add',$data,$fieldList);
			return json(['status'=>'00','msg'=>'操作成功']);
		}
	}
	
	/*修改数据*/
	function update(){
		if (!$this->request->isPost()){
			$extend_id =  $this->request->param('extend_id','','intval');
			$data_id =  $this->request->param('data_id','','intval');
			if(empty($extend_id) || empty($data_id)) $this->error('参数错误');
			$extendInfo = db('extend')->where('extend_id',$extend_id)->find();
			$extFormInfo = db($extendInfo['table_name'])->where('data_id',$data_id)->find();
			$htmlstr = '';
			$fieldList = db('field')->where(['extend_id'=>$extend_id,'status'=>1])->order('sortid asc')->select()->toArray();
			foreach($fieldList as $key=>$val){
				$val['value'] = html_out($extFormInfo[$val['field']]);
				$val['data_id'] = $data_id;
				$htmlstr .= \app\admin\service\FieldService::getFieldData($val);	
			}
			$this->view->assign('data_id',$data_id);
			$this->view->assign('extend_id',$extend_id);
			$this->view->assign('formStr',$htmlstr);
			$this->view->assign('fieldList',$fieldList);
			return view('form_data/info');
		}else{
			$data = $this->request->post();
			try {
				$fieldList = db('field')->where(['extend_id'=>$data['extend_id'],'status'=>1])->select();
				$res = FormDataService::saveData('edit',$data,$fieldList);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'操作成功']);
		}
	}
	
	
	/*获取拓展字段信息*/
	public function getExtends(){
		$extend_id =  input('param.extend_id','','intval');
		$fieldList =  db('field')->where(['extend_id'=>$extend_id,'status'=>1])->select();
		return json($fieldList);
	}
	

	

}