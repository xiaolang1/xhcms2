<?php
/**
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */
 
namespace app\admin\controller;
use think\exception\FuncNotFoundException;
use think\exception\ValidateException;
use app\BaseController;

class Admin extends BaseController
{
	
	public function initialize(){
		$controller = $this->request->controller();
		$action = $this->request->action();
		$app = app('http')->getName();
		
		$admin = session('admin');
        $userid = session('admin_sign') == data_auth_sign($admin) ? $admin['user_id'] : 0;
		
        if( !$userid && ( $app <> 'admin' || $controller <> 'Login' )){
			echo '<script type="text/javascript">window.parent.frames.location.href="'.url('admin/Login/index').'";</script>';exit();
        }

		$url =  "{$app}/{$controller}/{$action}";
		if(session('admin.role_id') <> 1 && !in_array($url,config('my.nocheck')) && $action !== 'getExtends' && $controller <> 'FormData'){	
			if(!in_array($url,session('admin.nodes'))){
				throw new ValidateException ('你没操作权限');
			}
		}
		
		event('DoLog');
		
		$list = db("config")->cache(true,60)->column('data','name');
		config($list,'xhadmin');
	}
	
	public function __call($method, $args){
        throw new FuncNotFoundException('方法不存在',$method);
    }

}
