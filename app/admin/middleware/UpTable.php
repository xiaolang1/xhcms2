<?php
/**
 * 更新数据表中间件
 * ============================================================================
 * * COPYRIGHT 2016-2019 xhadmin.com , and all rights reserved.
 * * WEBSITE: http://www.xhadmin.com;
 * ----------------------------------------------------------------------------
 * This is not a free software!You have not used for commercial purposes in the
 * premise of the program code to modify and use; and publication does not allow
 * any form of code for any purpose.
 * ============================================================================
 * Author: 寒塘冷月 QQ：274363574
 */

namespace app\admin\middleware;
use think\helper\Str;

class UpTable
{
	
    public function handle($request, \Closure $next)
    {	
		$data = $request->param();
		$extendInfo = db('extend')->where('extend_id',$data['extend_id'])->find();
		try{
			if($data['table_name'] && $data['table_name'] <> $extendInfo['table_name']){
				$sql="ALTER TABLE ".config('database.connections.mysql.prefix').config('my.create_table_pre').$extendInfo['table_name']." RENAME TO ".config('database.connections.mysql.prefix')."".$data['table_name'];
				db()->execute($sql);
			}		
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		
		return $next($request);
    }
	
	
	
}